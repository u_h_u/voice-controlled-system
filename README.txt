This is a README file for project "Voice controlled system".

Copyright (C) 2018 Uro� Hudomalj

The files in this repository are free to use: you can redistribute them and/or modify
them under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This files are distributed in the hope that they will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

The project describes how to develope a system for voice recognition and synthesis, based on a Arduino Due platform.

The most important parts of the repository are:
 - Glasovno_krmiljen_sistem_Uros_Hudomalj.docx : a description of the functioning of the system, its hardware and software (in Slovene)
 The code:
 - project/app : includes the project's source code (/src), includes (/includes) and code for the used operating system (/rtos)
 - project/asf : includes the used modules form Atmel, which were developed by Atmel Corporation and are the property of Atmel Corporation