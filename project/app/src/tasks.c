/*
 * tasks.c
 *
 *  Created on: Apr 19, 2017
 *      Author: user1
 */
#include <tasks.h>

/********************** LCD ********************/
rtos_task_t task_lcd = {	//dekleriramo task
	.function = lcd_driver,
	.last_tick = 0,
	.priority = 3,
	.interval = 20,
	.status = 1,
};

/********************** LEDS ********************/
rtos_task_t task_leds = {	//dekleriramo task
	.function = leds_driver,
	.last_tick = 0,
	.priority = 3,
	.interval = 5*500,
	.status = 1,
};

void leds_driver(void){
	static uint16_t stevec_status1 = 0;

	switch((int32_t)task_leds.status){
		case 1:
			//poslusa - LED2 utripa
			if(stevec_status1==3){
				led_toggle_state(1<<1);
			}
			stevec_status1++;
			stevec_status1 = stevec_status1 % 4;	//za periodo 12Hz
			break;
		case 2:
			//slisi zvok - LED1 on
			led_set_state(~0x0001);
			break;
		case 3:
			//racuna - LED3 utripa hitreje ko drugace LED2
			led_toggle_state(1<<2);
			break;
	}
}

/********************** BUTTONS ********************/

uint16_t buttons_fifo_buff[BUTTONS_FIFO_SIZE+1];
/* dekleracija buttons fifo */
fifo_t buttons_fifo = {
	0,
	0,
	BUTTONS_FIFO_SIZE+1,
	buttons_fifo_buff
};


rtos_task_t task_buttons = {	//dekleriramo task
	.function = buttons_driver,
	.last_tick = 0,
	.priority = 2,
	.interval = 5*250,
	.status = 1,
};

void buttons_driver(void){
	uint8_t button_press = get_button_press();

	//se odzovemo glede na pritisnjeno tipko

	//ce je prva, povecamo ure
	if(button_press & (1<<0)){
		hours++;
		hours = hours%24;
	}
	//ce je druga, povecamo min
	if(button_press & (1<<1)){
		minutes++;
		if(minutes==60){
			minutes=0;
			hours++;
			hours = hours%24;
		}
	}
	//ce je tretja, zmanjsamo minute
	if(button_press & (1<<2)){
		minutes--;
		if(minutes==-1){
			minutes=59;
			hours--;
			if(hours==-1){
				hours = 23;
			}
		}
	}
	//spremenimo stanje luci (LED 4)
	if(button_press & (1<<3)){
		ioport_toggle_pin_level(D4);	//ker je posebej od test board, da niso tezave z ostalimi
	}

}

/********************** CLOCK ********************/
rtos_task_t task_clock = {	//dekleriramo task
	.function = clock_driver,
	.last_tick = 0,
	.priority = 4,
	.interval = 5*1000,
	.status = 1,
};

void clock_driver(void){
	static uint16_t stevec = 0;


	//vpisemo trenutno stanje tipk v buffer
	if(stevec == 3*60){
		stevec = 0;
		minutes++;
		if(minutes==60){
			minutes=0;
			hours++;
			if(hours==24){
				hours=0;
			}
		}
	}

	stevec++;
}

/********************** ADC ********************/
uint16_t adc_fifo_buff[ADC_FIFO_SIZE+1];
/* dekleracija adc fifo */
fifo_t adc_fifo = {
	0,
	0,
	ADC_FIFO_SIZE+1,
	adc_fifo_buff
};

rtos_task_t task_adc = {	//dekleriramo task za vzorcenje vhodnega signala
	.function = adc_driver,
	.last_tick = 0,
	.priority = 0,			//ima najvisjo prioriteto
	.interval = 5,			
	.status = 1,
};

/* driver, le vzorci vhodni signal na adc in ga vpise v fifo */
void adc_driver(void){
	uint8_t n = 0;

	uint16_t adc_val = adc_read();

	adc_val = (adc_val%4048);	//da ga ne zabije kar nekam

	//ko preberemo nov vzorec, ga se filtriramo s korekcijskim LP IIR filtrom <- preveri, ce je dovolj hitro!!!
	adc_val = iir_filter(adc_val);

	n = fifo_write(&adc_fifo, &adc_val, 1);

	if(n==0){
		;//nekaj naredi?, ker sem zafilal buffer
		//buttons_fifo.write_idx = 0;
		//buttons_fifo.read_idx = 0;
		led_set_state(~0x0002);
		while(1);
	}

}

/********************** DAC ********************/
uint16_t dac_fifo_buff[DAC_FIFO_SIZE+1];	//fifo pravzaprav ne rabimo, saj bomo le brali iz nekega arraya, ki se ne bo spreminjal med izvajanjem taska!
/* dekleracija dac fifo */
fifo_t dac_fifo = {
	0,
	0,
	DAC_FIFO_SIZE+1,
	dac_fifo_buff
};

rtos_task_t task_dac = {	//dekleriramo task za vzorcenje vhodnega signala
	.function = dac_driver,
	.last_tick = 0,
	.priority = 0,			//ima najvisjo prioriteto
	.interval = 5,			
	.status = 0,
};

/* driver, le da na izhodu dac vrednost posnetega signala */
void dac_driver(void){
/*	uint8_t n = 0;

	uint16_t dac_val;

	n = fifo_read(&dac_fifo, &dac_val, 1);

	if(n==0){
		;//nekaj naredi?, ker sem zafilal buffer
		//buttons_fifo.write_idx = 0;
		//buttons_fifo.read_idx = 0;
		led_set_state(~0x0002);
	}
	else{
		dac_write(dac_val);
	}*/

	static uint16_t sample=0;	//stevec vzorca signala, ki se predvaja

	dac_write(output_signal[sample]);	//damo na dac trenutni vzorec

	sample++;	//premaknemo se na naslednji vzorec signala
	//ce smo prisli s predvajanjem signala do konca, prekinemo z opravilom task_dac
	if(sample == output_signal_size){
		task_dac.status=0;
		sample=0;	//pripravimo se na naslednji vzorec
	}

}
