/*
 * signal_processor.c
 *
 *  Created on: Jun 18, 2017
 *      Author: user1
 */

#include <signal_processor.h>

/* izracuna moc signala na odseku */
float signal_power(uint16_t *signal, uint16_t signal_size){
	float power = 0;
	uint16_t i = 0;

	for(i=0; i<signal_size; i++){
		power += (int16_t)(signal[i]-2048)*(int16_t)(signal[i]-2048);
	}

	return power;
}

/* izracuna stevilo prehodov skozi niclo signala na odseku */
uint32_t signal_zeros(uint16_t *signal, uint16_t signal_size){
	uint32_t zeros = 0;
	uint16_t i = 0;
	uint16_t mean = signal_mean(signal, signal_size);

	for(i=0; i<signal_size; i++){
		if(i!=0){
			//pogledamo, kolikokrat se signalu spremeni predznak
			if( (int16_t)(signal[i-1] - mean)*(int16_t)(signal[i] - mean) < 0 ){		//bi bilo hitreje namesto mnozenja uporabiti nekaj primerjav ali je enako?
				zeros++;
			}
		}
	}

	return zeros;
}

/* izracuna povprecno vrednost signala na odseku */
uint16_t signal_mean(uint16_t *signal, uint16_t signal_size){
	uint32_t mean = 0;
	uint16_t i = 0;

	for(i=0; i<signal_size; i++){
		mean += signal[i];
	}

	return mean = mean/signal_size;
}

/* Korekcijski LP filter 8. reda */
const int16_t b_iir[4] = {5341, 9142, 15237, 28705};
const int16_t a_iir[4][2] = {{197, 15416},  {3178, 13573},  {-1540, 4778}, {-2294, 2008}};

uint16_t iir_filter(uint16_t sample){

	static uint32_t x[4][3] = {{0,0,0},{0,0,0},{0,0,0}}; // Past input memory array initialized to zero
	static uint32_t y[4][3] = {{0,0,0},{0,0,0},{0,0,0}}; // Past output memory array initialized to zero

	uint16_t i, n;

	// propagate past inputs and outputs
	 for(i = 0; i < IIR_ORDER/2; i++){
		 for(n=2; n>0; n--){
			x[i][n] = x[i][n-1];
			y[i][n] = y[i][n-1];
		 }
	 }

	 x[0][0] = sample; // Save the newest input value
	// Compute the remaining filter terms
	 for(i = 0; i < IIR_ORDER/2; i++){
		 y[i][0] = (x[i][0]*Q14 + x[i][1]*b_iir[i] + x[i][2]*Q14) - (y[i][1]*a_iir[i][0] + y[i][2]*a_iir[i][1]);
		 y[i][0] /= Q14;

		 // output from current filter stage becomes input for the next
		 if(i+1 < IIR_ORDER/2){
			 x[i+1][0] = y[i][0];
		 }
	 }

	 y[3][0] /= FILTER_GAIN; // Normalize the result

	 return ( y[3][0] ); // Return the filter output */
}

