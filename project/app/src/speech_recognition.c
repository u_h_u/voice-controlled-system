#include <speech_recognition.h>








/*################ Funkcije ################*/

//////PAZI, CE SI POVSOD SPROSTIL RES VES POMNILNIK!!!


/* compare neznani signal primerja z danimi refernecnimi in ga rzvrsti v razred po principu 1-NN
*    vhod predstavlja neznani signal, dolzina neznanega signala, zbirka z referencnimi signali, stevilo razlicnih ref signalov in stevilo enakih ref signalov, ter zbirko dolzin vseh ref signalov
*    izhod je indeks najbolj podobnega razreda - ce je -1, pomeni, da noben signal ni bil dovolj podoben
*
*   (kot globalne spremenljivke so pomembne:)
*     ref_signals - 2d zbirka znacilk referencnih signalov (vsak 2d zbirka)
*     ref_class_num - stevilo razlicnih razredov
*     ref_eq_num - stevilo signalov enakega razreda
*     ref_signals_size - velikost referencnih signalov znacilk
*/
int8_t compare(uint16_t *signal, uint16_t signal_size, const float **ref_signals, uint8_t ref_class_num, uint8_t ref_eq_num, const uint16_t *ref_signals_size){
	int16_t i_class, j_ref;

	float min_cost = 0;		//hrani podobnost med neznanim in najbolj podobnim ref signalom
	int8_t min_indx = -1;   //hrani indx razreda najbolj podobnega ref signala
	float tmp_cost = 0;		//hrani trenutno vrednost podobnosti med neznanim in trenutnim ref signalom

	/* dobimo vektor mfcc znacilk signala - velikost [signal_size/(HALF_WINDOW_SIZE)][MEL_COEFF] */
	float **signal_feat = signal2coeff(signal, signal_size);




	//za pridobivanje znacilk referencnih signalov
/*	rtos_disable();		//da nam rtos ne kvari posiljanja preko uart-a, ga za ta cas izklopimo
	uint16_t sample;	//stevec vzorca signala, ki se posilja
	sample = 0;
	while(sample != signal_size/(HALF_WINDOW_SIZE)){
		int iuart;
		for(iuart=0; iuart<MEL_COEFF; iuart++){
			uart_send_float(signal_feat[sample][iuart]);	//preko uart posljemo trenutni vzorec
		}
		sample++;	//premaknemo se na naslednji vzorec signala
	}
	uart_send_float(99.9);
	rtos_enable();		//vklopimo naprej rtos
*/




	const float *ref_feat;	//hrani znacilke trenutnega referencnega signala

	/* ga primerjamo z danimi referecnimi signali */
	for(i_class=0; i_class<ref_class_num; i_class++){   //med vsemimi razredi
		for(j_ref=0; j_ref<ref_eq_num; j_ref++){		//med ref signali znotraj razreda
			ref_feat = ref_signals[i_class*ref_eq_num + j_ref];		//znacilke trenutnega [i-ti razred][j-ti posnetek] referencnega signala

			tmp_cost = dtwarp(signal_feat, signal_size/(HALF_WINDOW_SIZE), ref_feat, ref_signals_size[i_class*ref_eq_num + j_ref]);	//izracunamo podobnost med neznanim in trenutnim ref signalom

			/* razvrstitev neznanega vzorca po principu 1-NN */
			//ce je prvi ref signal, ga shranimo kot najbolj podobnega
			if(i_class==0 && j_ref==0){
				min_cost = tmp_cost;
				min_indx = i_class;
			}
			else{
				//drugace pa le, ce je bolj podobne kot trenutno najbolj podobni
				if(tmp_cost < min_cost){
					min_cost = tmp_cost;
					min_indx = i_class;
				}
			}
		}
	}

	/* ce je tudi najbolj podoben signal bolj razlicen, kot pa je prag, vrnemo, da vzorca nismo uspeli razvrstiti */
	if(min_cost > MIN_SIMILARITY){
		min_indx = -1;
	}

	/* sprostimo pomnilnik - nastal iz zancilk neznanega signala */
	for(i_class=0; i_class<signal_size/(HALF_WINDOW_SIZE); i_class++){
		free(signal_feat[i_class]);
	}
	free(signal_feat);

	return min_indx;
}

/* dtwarp - vrne normirano vrednost podobnosti med vhodnim neznanim in referencnim signalom
*/
float dtwarp(float **signal, uint16_t signal_size, const float *ref, uint16_t ref_size){
	uint16_t i,j,k;
	float dmin=0;			//minimalna cena prehoda
	float tmp=0;
	uint8_t tmp_smer = 0;		//trenutna smer prehoda
	uint16_t dolzina = 1;	//dolzina najcenejse poti

	float **razdalje = (float **) malloc(sizeof(float*)*signal_size);   //matrika, ki hrani razdalje med zancilkami referencenga in neznanega signala
	uint8_t **prehod = (uint8_t **) malloc(sizeof(uint8_t*)*signal_size);   //matrika, ki hrani smer prehoda za najcenejsi prehod
	for(i=0; i<signal_size; i++){
		razdalje[i] = (float *) malloc(sizeof(float)*ref_size);
		prehod[i] = (uint8_t *) malloc(sizeof(uint8_t)*ref_size);
	}

	//izracunamo vse (kvadrate) razdalje med znacilkami (hkrati prepisemo kar ) - ni treba tocno vseh (spremeni, ce bo prepocasi)
	i=0;
	for(i=0; i<signal_size; i++){
		for(j=0; j<ref_size; j++){
			razdalje[i][j] = 0;
			prehod[i][j] = 0;
			for(k=0; k<MEL_COEFF; k++){
				tmp = signal[i][k]-ref[j*MEL_COEFF + k];

				if(tmp>1000 || tmp<-1000){
					razdalje[i][j] += 1000000;
				}
				else if(tmp<0.01 && tmp>-0.01){
					razdalje[i][j] += 0;
				}
				else{
					razdalje[i][j] += tmp*tmp;
				}
			}
		}
	}

	//poiscemo pot z najmanjso ceno
	for(i=0; i<signal_size; i++){
		for(j=0; j<ref_size; j++){
			//poiscemo najmanjsi mozen prehod
			if(i>0 && j>0){
				if(razdalje[i-1][j] > razdalje[i][j-1]){
					tmp = razdalje[i][j-1];
					tmp_smer = 3;
				}
				else{
					tmp = razdalje[i-1][j];
					tmp_smer = 2;
				}
				if(tmp > razdalje[i-1][j-1]){
					dmin = razdalje[i-1][j-1];
					tmp_smer = 1;
				}
				else{
					;
				}
			}
			else if(i>0){
				tmp = razdalje[i-1][j];
				tmp_smer = 2;
			}
			else if(j>0){
				tmp = razdalje[i][j-1];
				tmp_smer = 3;
			}

			razdalje[i][j] += dmin;
			prehod[i][j] = tmp_smer;
		}
	}

	//poiscemo se dolzino najcenejse poti
	i = signal_size-1;
	j = ref_size-1;
	while(i>0 && j>0){
		switch(prehod[i][j]){
			case 1:
				i--;
				j--;
				break;
			case 2:
				i--;
				break;
			case 3:
				j--;
				break;
			default:
				i--;
				j--;
				break;
		}
		dolzina++;
	}

	tmp = razdalje[signal_size-1][ref_size-1];
	if(tmp<0.01){
		tmp=0;
	}
	else{
		tmp = tmp/(dolzina*dolzina);//rezultat je najcenejsa pot normirana z dolzino te poti - kar s kvadratom dolzine, ker so razdalje kvadrati
	}


	for(i=0; i<signal_size; i++){
		free(razdalje[i]);
		free(prehod[i]);
	}
	free(razdalje);
	free(prehod);

	return tmp;
}

/* signal2coeff
*	 prejme kazalec na signal, za katerega izracuna koeficiente mel kepstruma
*	 vrne kazalec na zbirko nastalih, ki je dvodimenzonalna - [signal_size/WINDOW_SIZE][MEL_COEFF]
*/
float **signal2coeff(uint16_t *signal, uint16_t signal_size){

	uint16_t coeff_size = signal_size/(HALF_WINDOW_SIZE);	//stevilo dobljenih podoken (ker imamo 50% prekrivanje med zaporednima oknoma)

	/* pripravimo zbriko za zancilke, veliksoti [signal_size/WINDOW_SIZE][MEL_COEFF] */
	float **coeff = (float**) malloc(sizeof(float*)*coeff_size);
	uint16_t i, j;

	/* signalu najprej odstejemo njegovo enosmerno komponento, da dobimo le ac signal */
	int16_t *ac_signal = signal2ac(signal, signal_size);

	/* iz ac signala izluscimo posamicno okno, katero obdelamo - premikamo se po polovici okna naprej*/
	for(i=0; i<=signal_size-HALF_WINDOW_SIZE; i+=HALF_WINDOW_SIZE){

		int16_t *windowed = (int16_t*) malloc(sizeof(int16_t)*FFT_SIZE);		//hrani trenutno okno, najvecje dolzine FFT_SIZE vzorcev (pomembno za fft kasneje)

		/* okno mnozimo s Hannovim oknom */
		for(j=0; j<HALF_WINDOW_SIZE; j++){          //zaradi simetrije gremo le do polovice

			int32_t tmp = (int32_t) (ac_signal[i+j]*Hann_window_coeff[j]);
			tmp = (tmp>>16);
			windowed[j] = (int16_t) tmp;

			tmp = (ac_signal[i+(WINDOW_SIZE-1)-j]*Hann_window_coeff[j]);
			tmp = (tmp>>16);
			windowed[(WINDOW_SIZE-1)-j] = (int16_t) tmp;
		}

/*		for(j=0; j<WINDOW_SIZE; j++){
			printf("%d \t", windowed[j]);
		}
		printf("\n\n\n");
*/

		/* dodamo nicle (zero padding) oknu, da bo dolzine 512 */

		for(j=0; j<FFT_SIZE-WINDOW_SIZE; j++){
			windowed[WINDOW_SIZE+j] = 0;
		}

		/* izracunamo mocnostni spekter oknjenega signala */
		float *power = fft512(windowed);
		free(windowed);

/*
		for(j=0; j<FFT_SIZE; j++){
			printf("%4.1f \t", power[j]);
		}
		printf("\n\n\n");
*/


      /*//////////////// VSE TESTIRANO Z MATLABOM IN DELA ODLICNO \\\\\\\\\\\\\\\\*/
      /*/////////////  edino power2mel nisem znal testirati - poglej!!! \\\\\\\\\\\\*/

		/* mocnostni spekter preslikamo na mel skalo - dobimo povprecne moci na odsekih mocnostnega spektra */
		float *mel = power2mel(power);
		free(power);

/*
		for(j=0; j<MEL_FILTER_NUM; j++){
			printf("%.1f, \t", mel[j]);
		}
		printf("\n\n\n");
*/


		/* na log10 signala iz povprecnih moci na mel lestvici izvedemo dct */
		float *window_features = mfcc(mel, MEL_FILTER_NUM, MEL_COEFF);
		free(mel);


/*
		for(j=0; j<MEL_COEFF; j++){
			printf("%.1f, \t", window_features[j]);
		}
		printf("\n\n\n");
*/

		/* sharnimo kazalec na znacilke trenutnega okna na ustrezno mesto */
		coeff[i/(HALF_WINDOW_SIZE)] = window_features;
	}

	free(ac_signal);

	return coeff;
}

/* signal2ac prejme signal, iz katerega izlusci le izmenicen del - tako da izracuna povprecje in ga odsteje od signala
*    vrne kazalec na novo ustvarjen signal, ki vsebuje le ac del
*/
int16_t *signal2ac(uint16_t *signal, uint16_t signal_size){
	uint16_t i;
	float vsota=0;
	uint16_t povprecje;

	int16_t *ac_signal = (int16_t*) malloc(sizeof(int16_t)*signal_size);

	for(i=0; i<signal_size; i++){
		vsota += signal[i];
	}

	povprecje = vsota/signal_size;

	for(i=0; i<signal_size; i++){
		ac_signal[i] = (int16_t) (signal[i]-povprecje);
	}

	return ac_signal;
}


/* fft512 izracuna mocnostni spekter enega okna, podanega v real_signal - predpostavlja dolzino okna 512 vzorcev (po potrebi prej zero padding)
*   vrne kazalec na zbirko mocnostgnega signala
*/
float *fft512(int16_t *real_signal){

	cplx *out = (cplx*) malloc(sizeof(cplx)*FFT_SIZE);		//kompleksen signal, ki predstavlja izhod fft-ja

	_fft(real_signal, out, FFT_SIZE);

	float *power = (float*) malloc(sizeof(float)*FFT_SIZE);	//mocnostni spekter vhodnega signala

	for(int i=0; i<FFT_SIZE; i++){
		//printf("%.1f, %.1f\n", out[i].r,out[i].i);
		power[i] = (out[i].r/FFT_SIZE*out[i].r/FFT_SIZE + out[i].i/FFT_SIZE*out[i].i/FFT_SIZE);	//izracunamo moc spektra kot re^2+im^2
		//printf("%8.1f \n", power[i]);
	}

	free(out);

	return power;
}

/* _fft izracuna fft signala in; za izracun je uporabljena moja implementacija Radix-2 algortima
*	 fft signala se shrani v out
*/
void _fft(int16_t *in, cplx *out, uint16_t N){
	int i;
	float fi;
	cplx *oute, *outo;
	int16_t *ine, *ino;

	cplx tmp;

	if(N==1){
		out[0].r=in[0];
		out[0].i=0;
	}
	else{

		oute = (cplx *) malloc(sizeof(cplx)*N/2);
		outo = (cplx *) malloc(sizeof(cplx)*N/2);

		ine = (int16_t *) malloc(sizeof(int16_t)*N/2);
		ino = (int16_t *) malloc(sizeof(int16_t)*N/2);

		for(i=0; i<N/2; i++){
			ine[i] = in[2*i];
			ino[i] = in[2*i+1];
		}

		_fft(ine, oute, N/2);
		_fft(ino, outo, N/2);

		for(i=0; i<N/2; i++){
			tmp.r = oute[i].r;
			tmp.i = oute[i].i;

			fi = 2*Pi*i/N;

			out[i].r = tmp.r + cos(fi)*outo[i].r + sin(fi)*outo[i].i;
			out[i].i = tmp.i - sin(fi)*outo[i].r + cos(fi)*outo[i].i;

			out[i+N/2].r = tmp.r - cos(fi)*outo[i].r - sin(fi)*outo[i].i;
			out[i+N/2].i = tmp.i + sin(fi)*outo[i].r - cos(fi)*outo[i].i;
		}

		free(oute);
		free(outo);

		free(ine);
		free(ino);

	}
}

/* power2mel izracuna iz mocnostenga spektra signala povprecne moci na odsekih, ki so med sabo razmaknjene po melodicni lestvici
*    vhod je kazalec na zbirko mocnostnega spektra (velikosti FFT_SIZE = 512)
*    izhod je kazalec na novo zbirko s povprecnimi mocmi
*/
float *power2mel(float *power){
	uint16_t i,j;
	float utez;	//trenutna utez prispevka signala

	float *mel = (float *) malloc(sizeof(float)*MEL_FILTER_NUM);	//izhodni vektor, ki vsebuje moci na odsekih signala, ki so melodicno razporejeni med sabo

	for(i=0; i<MEL_FILTER_NUM; i++){		//pogledamo za vsak BP filter, katere vrednosti padejo notri in dolocimo utezeno povprecno moc  (od 1. centralne pa do predzadnje centralne frekvence)
		mel[i] = 0;

		for(j=mel_ferq_bank[i]; j<=mel_ferq_bank[i+2]; j++){     //dovolj je, da gledamo frekvence med prejsnjo in naslednjo centralno frekvenco
			if(power[j]<0.01){    //da po nepotrebnem se ne mudimo z majhnimi vrednostmi
				continue;
			}
			//dolocimo utez filtra za posamicno komponento - trikotni potek filtra
			if(j<mel_ferq_bank[i+1]){
				utez = ((float)j-mel_ferq_bank[i]) / (mel_ferq_bank[i+1]-mel_ferq_bank[i]);
			}
			else if(j==mel_ferq_bank[i+1]){
				utez = 1;
			}
			else{
				utez = ((float)mel_ferq_bank[i+2] - j) / (mel_ferq_bank[i+2]-mel_ferq_bank[i+1]);
			}

			utez = utez*power[j];
			if(utez>0.01){    //da po nepotrebnem se ne mudimo z majhnimi vrednostmi
				mel[i] += utez;
			}
		}
	}

	return mel;
}

/* mfcc izvede diskretno kosinusno transformacijo na vhodnem signalu  (da dobis taksne rezultate kot v matlabu moras se mnoziti z sqrt(2/signal_size), kar tu ne naredim, ker ni potrebno - cas)
*    vrne kazalec na novo zbirko s dct transformacijo
*    kot vhod prejme dolzino signala
*    ter stevilo koeficientov mfcc, ki naj jih izracuna (0. izpustimo ker se nanasa na glasnost)
*/
float *mfcc(float *signal, uint8_t signal_size, uint8_t num_mfcc){
	float *dct = (float *) malloc(sizeof(float)*num_mfcc);
	float tmp;

	uint8_t i, j;

	for(i=1; i<=num_mfcc; i++){
		dct[i-1] = 0;
		for(j=0; j<signal_size; j++){
			tmp = log10(signal[j]+0.001)*cos(Pi/signal_size*(j+1.0/2)*i);

			dct[i-1] += tmp;
		}
	}

	return dct;
}
