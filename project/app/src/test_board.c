#include <test_board.h>


//----------------------------------------------------
// --------- Funkcije, ki se ticejo ADC --------------
void adc_setup(void){
	//inicializira uro za ADC
	sysclk_enable_peripheral_clock(ID_ADC);

	//inicializira adc
	adc_init(ADC, SystemCoreClock, ADC_clock, ADC_STARTUP_TIME_4);

	adc_configure_timing(ADC, 0, ADC_SETTLING_TIME_3, 1);

	adc_set_resolution(ADC, ADC_12_BITS );

	adc_configure_trigger(ADC, ADC_TRIG_SW, ADC_MR_FREERUN_OFF);

	adc_enable_channel(ADC, A0);
}

uint32_t adc_read(void){
	adc_start(ADC);

	uint32_t status = 0;

	while(status == 0){
		status = adc_get_status(ADC);
	}

	return adc_get_channel_value(ADC, A0);
}
//----------------------------------------------------
//----------------------------------------------------


//----------------------------------------------------
// --------- Funkcije, ki se ticejo DAC --------------
void dac_setup(void){
	sysclk_enable_peripheral_clock(ID_DACC);
	dacc_reset(DACC);	//za zacetek resetiramo dacc, ce je slucajno se kaj notri ostalo
	dacc_set_transfer_mode(DACC, DACC_MR_WORD_HALF);	//nastavimo, da je nas podatek le na 16 bitih od 32 bitov registra
	dacc_set_timing(DACC, 0x08, 0, 0x10);
	dacc_set_channel_selection(DACC, 1);
	dacc_enable_channel(DACC, 1);	//ze tudi pravilno postavi kanale pinov
}

void dac_write(uint16_t dac_val){
	/* predno vpisemo novo vrednost, moramo pocakati,
	 * ali je dacc prost za nov vnos (bit na 1)
	 * zato pogledamo 9. bit registra DACC_ISR
	 * */
	while( (dacc_get_interrupt_status(DACC) & DACC_ISR_TXRDY) == 0 );

	//vpisemo novo vrednost
	dacc_write_conversion_data(DACC, dac_val);
}
//----------------------------------------------------
//----------------------------------------------------

//----------------------------------------------------
//------------- Funkcije, ki se ticejo tipk ----------
int get_button_press(void){
	int out;
	int state = 0x0;
	static int state_old = 0xFFFFFFFF;

	state = get_button_state();
	out = (state_old ^ state) & state_old;		//ce se je stanje spremenilo, zaznamo s XOR, pri cemer mora biti stanje drugacno kot prej
	state_old = state;

	return out;
}

int get_button_state(void){
	int state = 0xFFFFFFFF;

	if(ioport_get_pin_level(T4) == 0){
		state &= 0xFFFFFFF7;
	}
	if(ioport_get_pin_level(T3) == 0){
		state &= 0xFFFFFFFB;
	}
	if(ioport_get_pin_level(T2) == 0){
		state &= 0xFFFFFFFD;
	}
	if(ioport_get_pin_level(T1) == 0){
		state &= 0xFFFFFFFE;
	}

	/*Druga opcija - postavljamo prebrane vrednosti na prave bite
		state = 0xFFFFFFF0;
		state = state | (ioport_get_level(T1) << 0) | (ioport_get_level(T1) << 1) | (ioport_get_level(T1) << 2) | (ioport_get_level(T1) << 3);

	*/
	return state;
}


void init_buttons(void){
	ioport_init();

	ioport_set_pin_dir(T1, IOPORT_DIR_INPUT);
	ioport_set_pin_dir(T2, IOPORT_DIR_INPUT);
	ioport_set_pin_dir(T3, IOPORT_DIR_INPUT);
	ioport_set_pin_dir(T4, IOPORT_DIR_INPUT);

	ioport_set_pin_mode(T1, IOPORT_MODE_PULLUP | IOPORT_MODE_DEBOUNCE);
	ioport_set_pin_mode(T2, IOPORT_MODE_PULLUP | IOPORT_MODE_DEBOUNCE);
	ioport_set_pin_mode(T3, IOPORT_MODE_PULLUP | IOPORT_MODE_DEBOUNCE);
	ioport_set_pin_mode(T4, IOPORT_MODE_PULLUP | IOPORT_MODE_DEBOUNCE);
}
//----------------------------------------------------
//----------------------------------------------------


//----------------------------------------------------
// --------- Funkcije, ki se ticejo LED --------------
void init_leds(void){
	ioport_init();

	ioport_set_pin_dir(D1, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(D2, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(D3, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(D4, IOPORT_DIR_OUTPUT);
}


void led_set_state(int state){
	ioport_set_pin_level(D1, !(0x00000001 & state));	//enako tudi 1<<0
	ioport_set_pin_level(D2, !(0x00000002 & state));	//enako tudi 1<<1
	ioport_set_pin_level(D3, !(0x00000004 & state));	//enako tudi 1<<2
//ker je posebej lucka	ioport_set_pin_level(D4, !(0x00000008 & state));	//enako tudi 1<<3
}

void led_toggle_state(int state){
	if( (state & (1<<0) ) !=0 ){
		ioport_toggle_pin_level(D1);
	}
	if( (state & (1<<1) ) !=0 ){
		ioport_toggle_pin_level(D2);
	}
	if( (state & (1<<2) ) !=0 ){
		ioport_toggle_pin_level(D3);
	}
	if( (state & (1<<3) ) !=0 ){
		ioport_toggle_pin_level(D4);
	}
}
//----------------------------------------------------
//----------------------------------------------------


//----------------------------------------------------
// --------- Funkcije, ki se ticejo LCD --------------
void str2lcd(uint32_t position, uint8_t *str){

	//v 32 presledkov prepise od vkljucno pozicije naprej str
	int i;
	for(i=0; str[i] != 0; i++){
		lcd_string[ (position+i)%32 ] = str[i];
	}
}
//----------------------------------------------------
//----------------------------------------------------





//----------------------------------------------------
// --------- Funkcije, ki se ticejo UART --------------
void uart_setup(void){
	/*------------ PIOA ----------*/
	//vklopimo uro (napajanje) za periferno enoto na POIA, kjer sta Rx in Tx liniji
	sysclk_enable_peripheral_clock(ID_PIOA);
	//nastavimo funkcijo preiferne enote na PA8 - Rx in PA9 - Tx (s set_peripheral odklopimo pinov od pioa porta, zato moramo nataviti le NVIC prekinitve od uart)
	pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA8A_URXD | PIO_PA9A_UTXD);
	//za UART komunikacijo, ne sme biti linija nikoli v zraku -> nastavimo pull-up upre na teh pinih
	pio_pull_up(PIOA, PIO_PA8A_URXD | PIO_PA9A_UTXD, PIO_PULLUP);

	/*----- UART -----*/
	//vklopimo uro (napajanje) za periferno enoto UART
	sysclk_enable_peripheral_clock(ID_UART);
	//v strukturo za uart vnesemo zelene karakteristike
	sam_uart_opt_t uart_opts = {
		.ul_mck = SystemCoreClock, 			// master clock
		.ul_baudrate = 115200, 				// hitrost prenosa
		.ul_mode = UART_MR_CHMODE_NORMAL | UART_MR_PAR_NO	//brez paritete in normalen mode - glej datasheet uart
	};
	//za zacetek resetiramo, ce je slucajno kaj ostalo od prej
	uart_reset(UART);		//vzame napajanje
	uart_reset_status(UART);	//pobrise le zastavice
	//inicializiramo uart, ki zelene karakteristike le vpise v pravilne registre
	uart_init(UART, &uart_opts);
	//vklopimo uart (Rx in Tx)
	uart_enable(UART);
}


/* uart_send_float poslje float v obliki 5.1 */
void uart_send_float(float data){
	uint8_t poslano_ok=0;
	uint8_t poslji=0;
	float potenca = 100;
	uint8_t i=0;
	uint8_t ze_nenicelna=0;

	//posljemo predznak
	if(data<0){
		//poslji minus
		do{
			poslano_ok=uart_write(UART, 0x2D);
		}while(poslano_ok!=0);
		data = data*(-1);
	}

	for(i=0; i<3; i++){
		poslji = (uint8_t) (data/potenca);	//izlocimo stotice, nato desetice, enice in na koncu desetinko
		poslji += 48;	//pretvorimo stevilko v char (ASCII znak za 0 je 48)

		if(poslji==48 && i<2 && ze_nenicelna==0){	//da ne posiljamo na zacetku nicel
			;
		}
		else{
			ze_nenicelna=1;
			do{
				poslano_ok=uart_write(UART, poslji);
			}while(poslano_ok!=0);
		}

		data = data - (poslji-48)*potenca;
		potenca /= 10;
	}

	//decimalna pika
	do{
		poslano_ok=uart_write(UART, 0x2E);
	}while(poslano_ok!=0);


	//desetinka
	poslji = (uint8_t) (data/potenca);	//izlocimo stotice, nato desetice, enice in na koncu desetinko
	poslji += 48;	//pretvorimo stevilko v char (ASCII znak za 0 je 48)
	do{
		poslano_ok=uart_write(UART, poslji);
	}while(poslano_ok!=0);



	//posljemo se vejico (0x2C) za konec
	do{
		poslano_ok=uart_write(UART, 0x2C);
	}while(poslano_ok!=0);
}
//----------------------------------------------------
//----------------------------------------------------
