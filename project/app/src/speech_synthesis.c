#include <speech_synthesis.h>


/*################ Funkcije ################*/
void say_time(){

	//nastavimo izhodni signal na trenutno uro
	if(hours<=20){
		output_signal = pos_stevilo[hours];
		output_signal_size = pos_stevilo_size[hours];
		//vklopimo task dac
		task_dac.status=1;
		//cakamo, da se predvaja celoten posnetek signala
		while(task_dac.status==1);
	}
	else{
		//recemo ostanek pri deljenju z 20
		output_signal = pos_stevilo[hours%20];
		output_signal_size = pos_stevilo_size[hours%20];
		//vklopimo task dac
		task_dac.status=1;
		//cakamo, da se predvaja celoten posnetek signala
		while(task_dac.status==1);

		//dodamo se in
		output_signal = pos_in;
		output_signal_size = pos_in_size;
		//vklopimo task dac
		task_dac.status=1;
		//cakamo, da se predvaja celoten posnetek signala
		while(task_dac.status==1);

		//dvajset
		output_signal = pos_stevilo[20];
		output_signal_size = pos_stevilo_size[20];
		//vklopimo task dac
		task_dac.status=1;
		//cakamo, da se predvaja celoten posnetek signala
		while(task_dac.status==1);
	}

	//dodamo se in
	output_signal = pos_in;
	output_signal_size = pos_in_size;
	//vklopimo task dac
	task_dac.status=1;
	//cakamo, da se predvaja celoten posnetek signala
	while(task_dac.status==1);

	//ponovimo za trenutne minute
	if(minutes<=20){
		output_signal = pos_stevilo[minutes];
		output_signal_size = pos_stevilo_size[minutes];
		//vklopimo task dac
		task_dac.status=1;
		//cakamo, da se predvaja celoten posnetek signala
		while(task_dac.status==1);
	}
	else if(minutes%10 == 0){
		//izgovorimo le mnogokratnik deset
		output_signal = pos_stevilo[minutes/10];
		output_signal_size = pos_stevilo_size[minutes/10];
		//vklopimo task dac
		task_dac.status=1;
		//cakamo, da se predvaja celoten posnetek signala
		while(task_dac.status==1);

		//ter besedo deset
		output_signal = pos_stevilo[10];
		output_signal_size = pos_stevilo_size[10];
		//vklopimo task dac
		task_dac.status=1;
		//cakamo, da se predvaja celoten posnetek signala
		while(task_dac.status==1);
	}
	else{
		//izgovorimo enice
		output_signal = pos_stevilo[minutes%10];
		output_signal_size = pos_stevilo_size[minutes%10];
		//vklopimo task dac
		task_dac.status=1;
		//cakamo, da se predvaja celoten posnetek signala
		while(task_dac.status==1);

		//besedo in
		output_signal = pos_in;
		output_signal_size = pos_in_size;
		//vklopimo task dac
		task_dac.status=1;
		//cakamo, da se predvaja celoten posnetek signala
		while(task_dac.status==1);

		//dodamo desetice
		output_signal = pos_stevilo[minutes/10];
		output_signal_size = pos_stevilo_size[minutes/10];
		//vklopimo task dac
		task_dac.status=1;
		//cakamo, da se predvaja celoten posnetek signala
		while(task_dac.status==1);

		//ter besedo deset
		output_signal = pos_stevilo[10];
		output_signal_size = pos_stevilo_size[10];
		//vklopimo task dac
		task_dac.status=1;
		//cakamo, da se predvaja celoten posnetek signala
		while(task_dac.status==1);

	}

	//ter besedo minut
	output_signal = pos_minut;
	output_signal_size = pos_minut_size;
	//vklopimo task dac
	task_dac.status=1;
	//cakamo, da se predvaja celoten posnetek signala
	while(task_dac.status==1);
}
