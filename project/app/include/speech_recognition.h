#ifndef SPEECH_RECOGNITION_H_
#define SPEECH_RECOGNITION_H_

/*///////////// INCLUDES \\\\\\\\\\\\\\\*/

#include <asf.h>
#include <math.h>
#include <signal_processor.h>
#include <rtos.h>
#include <test_board.h>

/*///////////// DEFINES \\\\\\\\\\\\\\\*/

#ifndef WINDOW_SIZE
	#define WINDOW_SIZE 384						//velikost enega okna
	#define HALF_WINDOW_SIZE WINDOW_SIZE/2		//velikost polovice okna
#endif

#define MEL_COEFF	13		//stevilo zancilk melodicnega spektra za eno okno
#define MEL_FILTER_NUM	24  //stevilo filtrov melodicne lestvice
#define MIN_SIMILARITY 10	//najmanjsa dovoljena podobnost med neznanim signalom in ref, da ga se stejemo kot znanega (in ne takega, ki ga nimamo sploh v ref bazi)
#define FFT_SIZE   512    // velikost of fft-ja

#define Pi 3.1415926

typedef struct {
	float r;
	float i;
} cplx;


////Za posamicne besede

//FIFI
#define REF_CLASS_NUM_FIFI 1	//stevilo razlicnih razredov
#define REF_EQ_NUM_FIFI 6		//stevilo signalov iz istega razreda

//LUC/URA
#define REF_CLASS_NUM_LUC_URA 2	//stevilo razlicnih razredov
#define REF_EQ_NUM_LUC_URA 6	//stevilo signalov iz istega razreda

//DA/NE
#define REF_CLASS_NUM_DA_NE 2	//stevilo razlicnih razredov
#define REF_EQ_NUM_DA_NE 6		//stevilo signalov iz istega razreda


/*///////////// GLOBALNE KONSTANTNE SPREMENLJIVKE \\\\\\\\\\\\\\\*/

extern const uint16_t Hann_window_coeff[WINDOW_SIZE/2];
extern const uint8_t mel_ferq_bank[MEL_FILTER_NUM+2];

/*/////////////// ZNACILKE REFERENCNIH SIGNALOV \\\\\\\\\\\\\\\\*/

//FIFI
extern const uint8_t ref_class_num_FIFI;	//stevilo razlicnih razredov
extern const uint8_t ref_eq_num_FIFI;			//stevilo signalov iz istega razreda
extern const uint16_t ref_signals_size_FIFI[REF_CLASS_NUM_FIFI*REF_EQ_NUM_FIFI];	//hrani dolzino signala znacilk posamicnega referencnega signala
extern const float *ref_signals_FIFI[REF_CLASS_NUM_FIFI*REF_EQ_NUM_FIFI];		//mi ni pustilo drugace!!! kot kar float ****

//LUC/URA
extern const uint8_t ref_class_num_LUC_URA;	//stevilo razlicnih razredov
extern const uint8_t ref_eq_num_LUC_URA ;			//stevilo signalov iz istega razreda
extern const uint16_t ref_signals_size_LUC_URA[REF_CLASS_NUM_LUC_URA*REF_EQ_NUM_LUC_URA];	//hrani dolzino signala znacilk posamicnega referencnega signala
extern const float *ref_signals_LUC_URA[REF_CLASS_NUM_LUC_URA*REF_EQ_NUM_LUC_URA];		//mi ni pustilo drugace!!! kot kar float ****

//DA/NE
extern const uint8_t ref_class_num_DA_NE;	//stevilo razlicnih razredov
extern const uint8_t ref_eq_num_DA_NE;			//stevilo signalov iz istega razreda
extern const uint16_t ref_signals_size_DA_NE[REF_CLASS_NUM_DA_NE*REF_EQ_NUM_DA_NE];	//hrani dolzino signala znacilk posamicnega referencnega signala
extern const float *ref_signals_DA_NE[REF_CLASS_NUM_DA_NE*REF_EQ_NUM_DA_NE];		//mi ni pustilo drugace!!! kot kar float ****



/*/////////////// FUNCTION PROTOTYPES \\\\\\\\\\\\\\\\*/

/* compare neznani signal primerja z danimi refernecnimi in ga rzvrsti v razred po principu 1-NN
*    vhod predstavlja neznani signal, dolzina neznanega signala, zbirka z referencnimi signali, stevilo razlicnih ref signalov in stevilo enakih ref signalov, ter zbirko dolzin vseh ref signalov
*    izhod je indeks najbolj podobnega razreda - ce je -1, pomeni, da noben signal ni bil dovolj podoben
*
*   (kot globalne spremenljivke so pomembne:)
*     ref_signals - 2d zbirka znacilk referencnih signalov (vsak 2d zbirka)
*     ref_class_num - stevilo razlicnih razredov
*     ref_eq_num - stevilo signalov enakega razreda
*     ref_signals_size - velikost referencnih signalov znacilk
*/
int8_t compare(uint16_t *signal, uint16_t signal_size, const float **ref_signals, uint8_t ref_class_num, uint8_t ref_eq_num, const uint16_t *ref_signals_size);

/* dtwarp - vrne normirano vrednost podobnosti med vhodnim neznanim in referencnim signalom
*/
float dtwarp(float **signal, uint16_t signal_size, const float *ref, uint16_t ref_size);

/* signal2coeff
*	 prejme kazalec na signal, za katerega izracuna koeficiente mel kepstruma
*	 vrne kazalec na zbirko nastalih, ki je dvodimenzonalna - [signal_size/WINDOW_SIZE][MEL_COEFF]
*/
float **signal2coeff(uint16_t *signal, uint16_t signal_size);

/* signal2ac prejme signal, iz katerega izlusci le izmenicen del - tako da izracuna povprecje in ga odsteje od signala
*    vrne kazalec na novo ustvarjen signal, ki vsebuje le ac del
*/
int16_t *signal2ac(uint16_t *signal, uint16_t signal_size);

/* fft512 izracuna mocnostni spekter enega okna, podanega v real_signal - predpostavlja dolzino okna 512 vzorcev (po potrebi prej zero padding)
*   vrne kazalec na zbirko mocnostgnega signala
*/
float *fft512(int16_t *real_signal);

/* _fft izracuna fft signala in; za izracun je uporabljena moja implementacija Radix-2 algortima
*	 fft signala se shrani v out
*/
void _fft(int16_t *in, cplx *out, uint16_t N);

/* power2mel izracuna iz mocnostenga spektra signala povprecne moci na odsekih, ki so med sabo razmaknjene po melodicni lestvici
*    vhod je kazalec na zbirko mocnostnega spektra (velikosti FFT_SIZE = 512)
*    izhod je kazalec na novo zbirko s povprecnimi mocmi
*/
float *power2mel(float *power);

/* mfcc izvede diskretno kosinusno transformacijo na vhodnem signalu  (da dobis taksne rezultate kot v matlabu moras se mnoziti z sqrt(2/signal_size), kar tu ne naredim, ker ni potrebno - cas)
*    vrne kazalec na novo zbirko s dct transformacijo
*    kot vhod prejme dolzino signala
*    ter stevilo koeficientov mfcc, ki naj jih izracuna (0. izpustimo ker se nanasa na glasnost)
*/
float *mfcc(float *signal, uint8_t signal_size, uint8_t num_mfcc);



#endif /* SPEECH_RECOGNITION_H_ */
