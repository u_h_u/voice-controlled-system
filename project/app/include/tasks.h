/*
 * tasks.h
 *
 *  Created on: Apr 19, 2017
 *      Author: user1
 */

#ifndef TASKS_H_
#define TASKS_H_

#include <rtos.h>			//da lahko uporabljamo iste strukture in typedef kot so v rtos
#include <test_board.h>		//da lahko uporabljamo funkcije (driverje) iz test_board
#include <signal_processor.h>

/* globalne zunanje spremenljivke */
extern uint8_t hours;
extern uint8_t minutes;

/********************** LCD ********************/
extern rtos_task_t task_lcd;	//povemo, da v nekem drugem filu obstaja ta spremenljivka -> posice linker

/********************** LEDS ********************/
extern rtos_task_t task_leds;
void leds_driver(void);

/********************** BUTTONS ********************/
#define BUTTONS_FIFO_SIZE 40

extern rtos_task_t task_buttons;
void buttons_driver(void);

/********************** CLOCK ********************/
extern rtos_task_t task_clock;
void clock_driver(void);

/********************** ADC ********************/
#define ADC_FIFO_SIZE 384*2

extern rtos_task_t task_adc;
extern fifo_t adc_fifo;
void adc_driver(void);

/********************** DAC ********************/
#define DAC_FIFO_SIZE 384*2

extern const uint16_t *output_signal;
extern uint16_t output_signal_size;

extern rtos_task_t task_dac;
extern fifo_t dac_fifo;
void dac_driver(void);

#endif /* TASKS_H_ */
