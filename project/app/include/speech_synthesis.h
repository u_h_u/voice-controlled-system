#ifndef SPEECH_SYNTHESIS_H_
#define SPEECH_SYNTHESIS_H_

/*///////////// INCLUDES \\\\\\\\\\\\\\\*/

#include <asf.h>
#include <tasks.h>


/*/////////////////// POSNETKI ZA PREDVAJATI \\\\\\\\\\\\\\\\\\\\*/

extern const uint16_t pos_zelite_size;
extern const uint16_t pos_zelite[];

extern const uint16_t pos_se_kaj_size;
extern const uint16_t pos_se_kaj[];

extern const uint16_t pos_ponovite_size;
extern const uint16_t pos_ponovite[];

extern const uint16_t pos_adijo_size;
extern const uint16_t pos_adijo[];

extern const uint16_t pos_in_size;
extern const uint16_t pos_in[];

extern const uint16_t pos_minut_size;
extern const uint16_t pos_minut[];

extern const uint16_t pos_stevilo_size[];
extern const uint16_t *pos_stevilo[];

/*/////////////// FUNCTION PROTOTYPES \\\\\\\\\\\\\\\\*/

extern uint8_t hours;
extern uint8_t minutes;

//extern const uint16_t *output_signal;
//extern uint16_t output_signal_size;

/* say_time - prebere trenutno stanje globalnih spremenljivk hours in minutes
 */
void say_time();

#endif /* SPEECH_SYNTHESIS_H_ */
