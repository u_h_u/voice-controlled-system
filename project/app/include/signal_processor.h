/*
 * signal_processor.h
 *
 *  Created on: Jun 18, 2017
 *      Author: user1
 */

#ifndef SIGNAL_PROCESSOR_H_
#define SIGNAL_PROCESSOR_H_

#include <asf.h>

/*########## DEFINES ###########*/
#define SAMPLING_FREQ_KHZ 24									//frekvenca vzorcenja v kHz <- izbrana tako, ker koristen signal do 8kHz, analogni del pa dovolj zadusi pri 16kHz
#define WINDOW_SIZE_MS 16										//velikost enega okna v ms
#define WINDOW_SIZE WINDOW_SIZE_MS*SAMPLING_FREQ_KHZ			//velikost enega okna (stevilo vzorcev)
#define HALF_WINDOW_SIZE WINDOW_SIZE/2							//velikost polovicenga okna
#define MAX_SIGNAL_SIZE 1000*SAMPLING_FREQ_KHZ					//vhodni signal, najdaljse dolzine 1s

#define NOISE_POWER 750000		//na so narejene reference 400000
#define NOISE_ZEROS 35


/*########## FUNCTIONS ###########*/
/* izracuna moc signala na odseku */
float signal_power(uint16_t *signal, uint16_t signal_size);

/* izracuna stevilo prehodov skozi niclo signala na odseku */
uint32_t signal_zeros(uint16_t *signal, uint16_t signal_size);

/* izracuna povprecno vrednost signala na odseku */
uint16_t signal_mean(uint16_t *signal, uint16_t signal_size);

/* Korekcijski LP filter 8. reda
 *  uint16_t sample - trenutni vzorec
 *  returns - pofiltriran vzorec
 *
 *  koeficienti so izbrani tako, da je:
 *   zgornja mejna frekvenca v prepustu je 8kHz pri vzorcenju z 24kHz
 *   spodnja mejna frekvenca v zapori je pri 8.8kHz, kjer pade signal na -40dB
 *   -40dB je tudi najmanjse dusenje v zapori
 *   v prepustu je vse do 7.4kHz ojacanje med 0 in 1dB, nato narste na 7dB, ko se bliza 8kHz
 *
 *	filter je izbran tako, da kompenzira posledice regularnega vzorcenja in vecje slabljenje analognega filtra okoli mejne frekvence
 * */
#define IIR_ORDER 8 // Filter order
#define Q14 (1<<14) // Scale factor
#define FILTER_GAIN 14	// 1/FILTER_GAIN - for faster computation use 16
/* Constants b0 – b2 and a1 – a2 as well as FILTERGAIN are defined as Q1.13 scaled signed integer 16 data types. */
//koeficineti polov za filter 8. reda
const int16_t b_iir[4];
//koeficienti nicel za filter 8. reda
const int16_t a_iir[4][2];

uint16_t iir_filter(uint16_t sample);


#endif /* SIGNAL_PROCESSOR_H_ */
