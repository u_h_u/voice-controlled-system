#ifndef TEST_BOARD_H_
#define TEST_BOARD_H_

#include <asf.h>
#include <lcd.h>
#include <adc.h>
#include <dacc.h>
#include <ioport.h>
#include <uart.h>

//-------------------
//-------------------
//------- ADC -------
#define A0 ADC_CHANNEL_7	//PIO_PA16_IDX
#define ADC_clock 6400000	//hitrost ADC ure
//vklopi funkcijo A/D pretvornika
void adc_setup(void);

uint32_t adc_read(void);
//-------------------
//-------------------

//-------------------
//------- DAC -------
#define DA0 ADC_CHANNEL_7	//PIO_PA16_IDX
//vklopi funkcijo D/A pretvornika
void dac_setup(void);

//na izhod D/A da vrednost dac_val
void dac_write(uint16_t dac_val);
//-------------------
//-------------------

//--------------------
//------- Tipk -------
//imena tipk
#define T1 PIO_PC26_IDX
#define T2 PIO_PC25_IDX
#define T3 PIO_PC24_IDX
#define T4 PIO_PC23_IDX

//pogleda, ali je tipka pritisnjena (spremlja se fronto)
int get_button_press(void);

//prebere tipke in vrne njihovo stanje kot zadnji 4 biti (aktivna 0)
int get_button_state(void);

//nastavi pine kot vhode za tipke
void init_buttons(void);
//-------------------
//-------------------


//--------------------
//------- LED --------
//imena ledic
#define D1 PIO_PC22_IDX
#define D2 PIO_PC21_IDX
#define D3 PIO_PC29_IDX
#define D4 PIO_PD7_IDX

//nastavi pine kot izhode za led
void init_leds(void);

//postavi ledice glede na zadnje 4 bite spremenljivke state
void led_set_state(int state);

//toggla vrednosti na
void led_toggle_state(int state);
//-------------------
//-------------------


//--------------------
//------- LCD --------
void str2lcd(uint32_t position, uint8_t *str);
//-------------------
//-------------------


//--------------------
//------- UART -------
//inicializira UART komunikacijo
void uart_setup(void);
//poslje podatek data preko UART-a
void uart_send(uint16_t data);
//poslje float na 1 decimalko preko UART-a
void uart_send_float(float data);
//-------------------
//-------------------

#endif /* TEST_BOARD_H_ */
