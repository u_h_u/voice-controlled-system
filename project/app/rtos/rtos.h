/*
 * rtos.h
 *
 *  Created on: Apr 19, 2017
 *      Author: user1
 */

#ifndef RTOS_H_
#define RTOS_H_

#include <asf.h>


/*********************** RTOS ***********************/

/* definicija novega podatkovnega tipa, ki je kazalec na funkcijo */
typedef void (* ptr_function)(void);

/* struktura, ki opisuje nas task */		//_task je le vmesno ime; zaradi modularnosti uporabimo strukturo
struct _task{
	ptr_function function;	/* kazalec na funkcijo */

	uint32_t last_tick;		/* trenutek zadnjega zagona opravila */
	uint32_t priority;		/* prioriteta opravila - najvisja 0 do st. prioritet-1*/
	uint32_t interval;		/* interval, na katerega se more opravilo sproziti */
	uint32_t status;		/* status, kaj naj dela task - 0 je izkljucen */
};

/* definiramo nov podatkovni tip - struktura, ki opisuje nas task (le za manj tipkanja) */
typedef struct _task rtos_task_t;


/* rtos_init
 * 		inicializira rtos
 * 		slice_us: dolzina casovne rezine v mikrosekundah
 *		vrne: 0 ce uspe
 *			  1 ce ne uspe (ce je npr. predolga rezina, da bi jo lahko izvedli z nasim stevcem)
 */
uint32_t rtos_init(uint32_t slice_us);

/* vstavi SysTick timer*/
void rtos_disable(void);	//vstavi OS			(uporabno za npr. hkratni dostop do spremenljivk)
/* zazene SysTick timer*/
void rtos_enable(void);		//zazene OS

/* RTOS Scheduler
 * prekinitev, ki se izvede na definiran cas v rtos_init
 */
////void SysTick_Handler(void); prototip je ze napisan v asf


/****************************************************/


/*********************** FIFO ***********************/


/* fifo struktura - buffer, podatek za velikost bufferja in indeks pisanja ter branja */
struct _fifo {
	uint16_t read_idx;
	uint16_t write_idx;
	uint16_t size;
	uint16_t *buffer;		//ce bomo zeleli vpisati vecje podatke, bomo jih pac na vec mest
};

/* le novo poimenovanje */
typedef struct _fifo fifo_t;


/* fukncija za branje iz fifo
 *	fifo - naslov fifo, iz katerega zelimo prebrati	(podamo naslov, da lahko spreminjamo vrednosti v fifo!)
 *	data - kam zelimo, da vrne prebrane vrednosti		(mora biti 16 bitov, saj zelimo notri vpisati prebrane vrednosti iz adc z 12 bitno natancnostjo)
 *	n - koliko podatkov zelimo prebrati					(dovolj je 8 bitov, saj hkrati zelimo prebrati najvec le polovicno okno, ki je 128 oz. 256 vzorcev)
 *
 *	vrne stevilo, koliko podatkov je uspesno prebral	(ce je vse OK, mora biti enako stevilu n)
 */
uint8_t fifo_read(fifo_t *fifo, uint16_t *data, uint16_t n);
/* fukncija za pisanje v fifo
 *	fifo - naslov fifo, v katerega zelimo pisati
 *	data - kaj zelimo vpisati
 *	n - koliko podatkov zelimo vpisati
 *
 *	vrne stevilo, koliko podatkov je uspesno vpisal	    (ce je vse OK, mora biti enako stevilu n)
 */
uint8_t fifo_write(fifo_t *fifo, uint16_t *data, uint16_t n);


/****************************************************/


#endif /* RTOS_H_ */
