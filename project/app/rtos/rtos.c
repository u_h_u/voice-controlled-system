/*
 * rtos.c
 *
 *  Created on: Apr 19, 2017
 *      Author: user1
 */
#include <rtos.h>

/************************ RTOS **********************/
/* globalna spremenljivka, ki hrani dolzino slica*/ //(bi bilo bolje, da bi bila static???)
uint32_t rtos_slice;

uint32_t rtos_init(uint32_t slice_us){
	//ne zazene RTOS => uporabi rtos_enable

	/* pretvorimo zeljeni cas v us v stevilo ciklov */
	uint32_t ticks = 84000000 * (double)slice_us / 1000000 -1;

	rtos_slice = slice_us; //shranimo dolzino casovne rezine (pri inicializaciji) za nadaljnjo uporabo znotraj rtos.c

	if(ticks > (1<<24) -1) return 1;					//ne moremo do toliko steti -> javimo napako (ali pa kar while(1); <- to uredi drugje)
	if(ticks <= 0) return 1;							//prekratek cas

	NVIC_SetPriority (SysTick_IRQn, 0);  				/* nastavi prioriteto prekinitve - ni potrebno posebej vklapljati, saj je sistemska */
	SysTick->VAL  = 0;                                  /* Load the SysTick Counter Value */
	SysTick->LOAD = ticks;      						/* nastavimo do koliko bo stel stevec */

	SysTick->CTRL &= SysTick_CTRL_CLKSOURCE_Msk;		/* nastavimo uro -> 2. bit v registru na 1, da bo MCK (ce 0, je MCK/8) */
	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;	 		/* izklopimo stevec */
	SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;		 	/* vklopimo interupt */

	return 0;
}

void rtos_disable(void){
//uporabili bomo varianto 1. - do enable ustavimo stevec

	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;

}

void rtos_enable(void){
//zazene RTOS

	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;

}


extern rtos_task_t *rtos_task_list[];	//povemo, da je zunaj deklerirana zbirka opravil
extern uint32_t rtos_num_task;			//in velikost te zbirke
/* RTOS Scheduler: prekinitev, ki se izvede na definiran cas v rtos_init */
void SysTick_Handler(void){

	static uint32_t tick=0;
	rtos_task_t *current_task = NULL;

	uint32_t i = 0;

	for(i=0; i<rtos_num_task; i++){
		/* pregledamo, kaksen je status taska - ali je sploh enable (razlicen od nic) */
		if(rtos_task_list[i]->status != 0){
			/* nato pogledamo, ali je cas pravsnji za izvedbo taska */
			if(tick - rtos_task_list[i]->last_tick >= rtos_task_list[i]->interval){
				/* ce se nismo nobenega primernega taska nasli - da je vsaj en */
				if(current_task == NULL){
					current_task = rtos_task_list[i];
				}
				else{
					/* ce smo ze kaksnega nasli, pogledamo, ali ima trenutni task nizjo prioriteto kot prejsnji najden */
					/* pri tem pa tisti z nizjo prioriteto zamuja vec kot drugi */
					if( (rtos_task_list[i]->priority < current_task->priority) && ( (tick-rtos_task_list[i]->last_tick-rtos_task_list[i]->interval) >= (tick-current_task->last_tick-current_task->interval) ) ){
						current_task = rtos_task_list[i];
					}
				}
			}
		}
	}

	/* ce smo nasli vsaj enega, ki ga je potrebno izvesti, ga izvedemo*/
	if(current_task != NULL){
		current_task->function();
		current_task->last_tick=tick;
	}

	/* povecamo tick */
	tick++;

	/* preverimo, ce je bila slucajno prekoracena casovna rezina */
	if(SCB->ICSR & SCB_ICSR_PENDSTSET_Msk) while(1);	//v primeru prekoracitve, tu ujamemo program
}
/****************************************************/


/*********************** FIFO ***********************/



/* fukncija za branje iz fifo */
uint8_t fifo_read(fifo_t *fifo, uint16_t *data, uint16_t n){
	uint8_t i=0;

	for(i=0; i<n; i++){
		if(fifo->write_idx != fifo->read_idx){	//ce smo z branjem tam, kjer smo s pisanjem, je buffer prazen
			data[i] = fifo->buffer[ fifo->read_idx ]; //i-ti podatek iz data vpisemo na trenutno pisalno mesto v fifo
			fifo->read_idx = (fifo->read_idx+1)%fifo->size;	//premaknemo bralni indeks
		}
		else{
			break; //ce je buffer prazen, nehamo brat
		}
	}
	return i;
}

/* fukncija za pisanje v fifo */
uint8_t fifo_write(fifo_t *fifo, uint16_t *data, uint16_t n){
	uint8_t i=0;

	for(i=0; i<n; i++){
		uint16_t tmp_write_idx = (fifo->write_idx+1)%fifo->size;	//naslednji pisalni indeks

		if(tmp_write_idx != fifo->read_idx){	//ce je prostor za vpis, ga lahko vpisemo
			fifo->buffer[ fifo->write_idx ] = data[i]; //i-ti podatek iz data vpisemo na trenutno pisalno mesto v fifo
			fifo->write_idx = tmp_write_idx;	//premaknemo pisalni indeks
		}
		else{
			break; //ce smo zapolnili buffer, nehamo pisati
		}
	}
	return i;
}



/****************************************************/
